export default function ({store, route, redirect}){

  const user = store.state.user;
  const blockedRoute = /\/admin\/*/g;
  const homeRoute = "/Vuemmerce/";

    if (!user && route.path.match(blockedRoute)){
      redirect("/Vuemmerce/");
    }
    if (user && route.path === homeRoute){
      redirect("/Vuemmerce/");
    } else {
      return redirect(homeRoute);
    }

    // if (route.path !== '/auth/signin') {
    //   //we are on a protected route
    //   if(!app.$fire.auth.currentUser) {
    //     //take them to sign in page
    //     return redirect('/auth/signin')
    //   }
    // } else if (route.path === '/auth/signin') {
    //   if(!app.$fire.auth.currentUser) {
    //     //leave them on the sign in page
    //   } else {
    //     return redirect('/')
    //   }
    // }
  }
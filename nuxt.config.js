const { TRUE } = require("node-sass");
const pkg = require("./package");



module.exports = {
  target: 'static',
  
  ssr: false, // Disable Server Side rendering

  router: {
    base: "/Vuemmerce/",
    //base: "/Vuemmerce/admin",
    //middleware: ['authenticated']
  },

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.description,
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: pkg.description },
      { "http-equiv": "x-ua-compatible", content: "ie=edge" },
      { name: "msapplication-TileColor", content: "#ffffff" },
      { name: "msapplication-TileImage", content: "/ms-icon-144x144.png" },
      { name: "theme-color", content: "#ffffff" },

      // Facebook open graph
      { property: "og:type", content: "website" },
      { property: "og:url", content: "https://example.com/page.html" },
      { property: "og:title", content: "Content Title" },
      { property: "og:image", content: "https://example.com/image.jpg" },
      { property: "og:description", content: "Description Here" },
      { property: "og:site_name", content: "Site Name" },
      { property: "og:locale", content: "en_US" },

      // Twitter card
      { property: "twitter:card", content: "summary" },
      { property: "twitter:site", content: "@site_account" },
      { property: "twitter:creator", content: "@individual_account" },
      { property: "twitter:url", content: "https://example.com/page.html" },
      { property: "twitter:title", content: "Content Title" },
      {
        property: "twitter:description",
        content: "Content description less than 200 characters"
      },
      { property: "twitter:image", content: "https://example.com/image.jpg" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "icon",
        type: "image/png",
        sizes: "192x192",
        href: "/android-icon-192x192.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "96x96",
        href: "/favicon-96x96.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/favicon-16x16.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "57x57",
        href: "/apple-icon-57x57.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "60x60",
        href: "/apple-icon-60x60.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "72x72",
        href: "/apple-icon-72x72.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "76x76",
        href: "/apple-icon-76x76.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "114x114",
        href: "/apple-icon-114x114.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "120x120",
        href: "/apple-icon-120x120.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "144x144",
        href: "/apple-icon-144x144.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "152x152",
        href: "/apple-icon-152x152.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/apple-icon-180x180.png"
      },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      },
      {
        rel: "stylesheet",
        integrity:
          "sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p",
        crossorigin: "anonymous",
        href: "https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  /*
   ** Global CSS
   */
  css: ["bulma"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/firebase.js',
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
      '@nuxtjs/axios',

  ],
  /*
   ** Axios module configuration
   */
  axios: {
    proxy: true,
    retry: { retries: 3 },
    exposedHeaders: ["set-cookie"]
    // See https://github.com/nuxt-community/axios-module#options
  },

  // proxy:{
  //   // api1: Customer Svc
  //   '/api1/v1': {target: 'http://35.225.240.100:8080/', pathRewrite: {'/api1/v1': ''}},
  //   // api2: User Admin Svc
  //   '/api2/v1': {target: 'http://34.87.100.205/', pathRewrite: {'/api2/v1': ''}},
  //   // api3: Product Svc
  //   '/api3/v1': {target: 'http://34.87.100.205/', pathRewrite: {'/api3/v1': ''}},
  //   // api4: Cart Svc
  //   '/api4/v1': 'http://35.193.0.50:9090/',

  //   '/api5/v1': {target: 'http://bibekshakya.com/', pathRewrite: {'/api5/v1': ''}}

  // },

  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  },
  // comment out to generate distribution folder "/dist"
  // generate: {
  //   dir: "docs"
  // }
};

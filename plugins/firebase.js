// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import firebase from 'firebase/app';
// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/database";

var firebaseConfig = {
/*     apiKey: "AIzaSyAZSg-Q0AtexxITF9kb-Jp0s4_Y3980UfQ",
    authDomain: "vegan-ecommerce.firebaseapp.com",
    databaseURL: "https://vegan-ecommerce-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "vegan-ecommerce",
    storageBucket: "vegan-ecommerce.appspot.com",
    messagingSenderId: "158188572224",
    appId: "1:158188572224:web:e97901cc41cb417e5299a5",
    measurementId: "G-HY2QBX3BJ1" */
    // AuthenticationProject from Sean
    apiKey: "AIzaSyAa_eaZyIASoaImuSbPF_KWEF792Z-oytE",
    authDomain: "authenticationproject-35d06.firebaseapp.com",
    projectId: "authenticationproject-35d06",
    storageBucket: "authenticationproject-35d06.appspot.com",
    messagingSenderId: "729002591399",
    appId: "1:729002591399:web:5284ab288878020ed50782",
    measurementId: "G-BMRY4VPSDK"

};

// The following fields are REQUIRED:
//  - Project ID
//  - App ID
//  - API Key
const secondaryAppConfig = {
    // Project from Kelvin with CSRF and Access Control implementation
    apiKey: "AIzaSyAQGg6VnmX5256hqZviDxt32qcF14HxK5w",
    authDomain: "my-1st-project-323611.firebaseapp.com",
    databaseURL: "https://my-1st-project-323611-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "my-1st-project-323611",
    storageBucket: "my-1st-project-323611.appspot.com",
    messagingSenderId: "577471188629",
    appId: "1:577471188629:web:76a27a5ef2c5fae041159c"
};

let app = null;
// Initialize Firebase
if (!firebase.apps.length) {
    //let app = firebase.initializeApp(firebaseConfig);
    app = firebase.initializeApp(secondaryAppConfig);

    // Initialize another app with a different config
    //const secondaryApp = firebase.initializeApp(secondaryAppConfig, "secondary");

    // Access services, such as the Realtime Database
    // secondaryApp.database();
}

export default firebase;
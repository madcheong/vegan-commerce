import axios from "axios";
import Cookies from 'js-cookie';

export default{
    async getAllCustomer(token)
    {
        //let headerToken = JSON.parse(token);
        //let config = { headers: {"Authorization" : `Bearer ${headerToken}`}};
        //console.log("config :" + JSON.stringify(config));
        await axios.get('/api5/v1/demo/vue/switchable-grid/api/products.json').then((response)=>{
            console.log("Customer Service Response " + JSON.stringify(response.data))
            return { response }
        });
        //const res = apiClient.get('/api5/v1/customer' , config);
        //const res = await axios.get('/api5/v1' + 'demo/vue/switchable-grid/api/products.json');
 /*            .then(res => {
                console.log(res.data);
            }).catch((error) => {
                console.log(error) 
            }); */
        //console.log("GetAllCustomer response: " + res.data);
    },
    getCustomerById(user)
    {
        return apiClient.get('/api5/v1/demo/vue/switchable-grid/api/products.json' + user.id); 
    },
    async deleteCustomerById(user, token)
    {
        let config = {
        headers: {
            'Content-Type' : 'application/json',
            'Accept' : 'application/json',
            'Authorization' : 'Bearer '+ token
            }
        }
        console.log("Delete customer by Id :" + user.customerId);

        let response = { 'delete' : true };

        return response;
        /* await axios.delete('http://35.225.240.100:8080/customer/ID/' + user.customerId , config)
        .then(response =>{
            console.log(response.data);
            
            return JSON.stringify(response.data);
        }) */
    },
    async updateCustomer(user, token){
        let config = {
        headers: {
            'Content-Type' : 'application/json',
            'Accept' : 'application/json',
            'Authorization' : 'Bearer '+ token
            }
        }
        let bodyparam = {
            user,
        }

        console.log("Update User :" + JSON.stringify(user));

        console.log("Config :" + JSON.stringify(config));
        
        //return $axios.post('https://jsonplaceholder.typicode.com/posts')
        await axios.patch('http://35.225.240.100:8080/customer/ID/' + user.customerId, 
                        bodyparam,
                        config)
        .then(response =>{
            console.log(response.data);
            //localStorage.setItem("customerData", JSON.stringify(response.data));
            return { response }
        })

    },
    getAllInActive(){
        return apiClient.get('/api5/v1/demo/vue/switchable-grid/api/products.json' + user.id); 
    },
    getCustomerByUid(uid){
        return apiClient.get('/api5/v1/demo/vue/switchable-grid/api/products.json' + uid); 
    },
    deleteCustomerByUid(uid)
    {
        return apiClient.delete('/api5/v1/demo/vue/switchable-grid/api/products.json' + uid); 
    },
    async createCustomer(user, token){
        let config = {
        headers: {
            'Content-Type' : 'application/json',
            'Accept' : 'application/json',
            'Authorization' : 'Bearer '+ token
            }
        }

        console.log("Create User :" + JSON.stringify(user));
        console.log("Config :" + JSON.stringify(config));
        
        //return $axios.post('https://jsonplaceholder.typicode.com/posts')
        await axios.post('http://35.225.240.100:8080/customer', 
                        user,
                        config)
        .then(response =>{
            console.log(response.data);
            //localStorage.setItem("customerData", JSON.stringify(response.data));
            return { response }
        })

    },

}
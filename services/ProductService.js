import axios from 'axios'

const apiClient = axios.create({
    basedURL: '/api1/v1',
    withCredentials: false,
    headers:{
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }

})
export default{
    async update(context)
    {
        // retrieve jwt token that must be USER_ADMIN
        //let bearerToken = localStorage.getItem("encoded_accessToken");
        let bearerToken = JSON.parse(context.$store.getters.getAccessToken);
        let config = {
            headers: {'Authorization': 'Bearer ' + bearerToken}
        }
/*         const bodyParameters = {
            "claims": data.claims,
            "email": data.email
        }
        const response = await apiClient.post(
            '/api5/v1/addclaims',
            bodyParameters,
            config,
        ) */
        console.log("Product Service Update: " + data);

    },
    async delete(context)
    {
        // retrieve jwt token that must be USER_ADMIN
        //let bearerToken = localStorage.getItem("encoded_accessToken");
        let bearerToken = JSON.parse(context.$store.getters.getAccessToken);
        let config = {
            headers: {'Authorization': 'Bearer ' + bearerToken}
        }
/*         const bodyParameters = {
            "claims": data.claims,
            "email": data.email
        }
        const response = await apiClient.post(
            '/api5/v1/addclaims',
            bodyParameters,
            config,
        ) */
        console.log("Product Service Delete: " + data);

    },
}
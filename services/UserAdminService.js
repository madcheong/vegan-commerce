import axios from "axios"

const apiClient = axios.create({
    //basedURL: '/api5/v1/demo/vue/switchable-grid/api/products.json',
    basedURL: '/api5/v1/addclaims',
    withCredentials: false,
    headers:{
        'Authorization': 'Bearer ',
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
    }
})
export default{
    
    async setClaims(data)
    {
        // retrieve jwt token that must be USER_ADMIN
        //let bearerToken = localStorage.getItem("encoded_accessToken");
        let bearerToken = JSON.parse(this.$store.getters.getAccessToken);
        let config = {
            headers: {'Authorization': 'Bearer ' + bearerToken}
        }
        const bodyParameters = {
            "claims": data.claims,
            "email": data.email
        }
        const response = await apiClient.post(
            '/api5/v1/addclaims',
            bodyParameters,
            config,
        )
        console.log("Set Claims Resp: " + response.data);

    },
    removeClaims(data)
    {
        //return apiClient.put('/api5/v1/removeclaims'); 
        //return apiClient.get('/api5/v1/demo/vue/switchable-grid/api/products.json'); 
        // retrieve jwt token that must be USER_ADMIN
        //let bearerToken = localStorage.getItem("encoded_accessToken");
        let bearerToken = JSON.parse(this.$store.getters.getAccessToken);
        let config = {
            headers: {'Authorization': 'Bearer ' + bearerToken,}
        }
        const bodyParameters = {
            "claims": data.claims,
            "email": data.email
          }
        //prepare JSON body
        try {
            apiClient.put({
                
                basedURL: '/api5/v1/removeclaims',
                bodyParameters,
                config,

            }).then((response)=>{
                let res = response.data;
            })
        } catch (error) {
        console.error(error);
        // expected output: ReferenceError: nonExistentFunction is not defined
        // Note - error messages will vary depending on browser
        }
    },
    async getAccounts(config)
    {
        // retrieve jwt token that must be USER_ADMIN
        //let bearerToken = localStorage.getItem("encoded_accessToken");
        // let bearerToken = JSON.parse(context.store.getters.getAccessToken);
        // let config = {
        //     headers: {'Authorization': 'Bearer ' + bearerToken}
        // }
        const response = await apiClient.get(
            '/api2/v1/getAccounts',
            config,
        )
        //localStorage.setItem("usersAccount", JSON.stringify(response.data));
        //this.$store.commit('setUsersAccount', JSON.stringify(response.data));
        console.log("Get Accounts Resp: " + response.data);
        return response;
    },

}
// import JWTDecode from "jwt-decode";
// import cookieParser from "cookie-parser";


import firebase from 'firebase/app';
import jwt_decode from "jwt-decode";
import CustomerService from '@/services/CustomerService'

function sendVerification()
{
  var user = firebase.auth().currentUser;
  user.sendEmailVerification().then(()=> {

    window.alert("Verification Sent");
  }).catch(error=>{
    console.log(error);
  })

}
function getUserClaims(tokenResult)
{
  let jwtDecodedValue = jwt_decode(tokenResult);

  //console.log("Decoded JWT is : " + JSON.stringify(jwtDecodedValue));
  //console.log("getUserClaims Function :" + JSON.stringify(jwtDecodedValue.claims));

  //localStorage.setItem("decodedjwt", JSON.stringify(jwtDecodedValue));
  localStorage.setItem("userclaims", JSON.stringify(jwtDecodedValue.claims));

  // return in string
  return jwtDecodedValue.claims;
}

function storeUserClaims(role)
{
  console.log("Role is : " + role);
  switch(role)
  {
    case 'ROLE_USER':
      localStorage.setItem("isCustomer", true);
      break;
    case 'ROLE_MERCHANT':
      localStorage.setItem("isMerchant", true);
      break;
    case 'ROLE_ADMIN':
      localStorage.setItem("isAdmin", true);
      break;
    default:

  }
}

export const state = () => ({
  products: [
    {
      id: 1,
      title: 'Product 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 50,
      ratings: 3,
      reviews: 5,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 2,
      title: 'Product 2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 35,
      ratings: 5,
      reviews: 10,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 3,
      title: 'Product 3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 110,
      ratings: 2,
      reviews: 3,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 4,
      title: 'Product 4',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 50,
      ratings: 1,
      reviews: 0,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 5,
      title: 'Product 5',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 35,
      ratings: 4,
      reviews: 2,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 6,
      title: 'Product 6',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 110,
      ratings: 5,
      reviews: 1,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 7,
      title: 'Product 7',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 50,
      ratings: 5,
      reviews: 7,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 8,
      title: 'Product 8',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 35,
      ratings: 3,
      reviews: 0,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    },
    {
      id: 9,
      title: 'Product 9',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      price: 110,
      ratings: 4,
      reviews: 2,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1
    }
  ],
  allusers: [
    {
      id: 1,
      name: 'User 1',
      email: 'user1@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 2,
      name: 'User 2',
      email: 'user2@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 3,
      name: 'User 3',
      email: 'user3@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 4,
      name: 'User 4',
      email: 'user4@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 5,
      name: 'User 5',
      email: 'user5@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 6,
      name: 'User 6',
      email: 'user6@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 7,
      name: 'User 7',
      email: 'user7@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 8,
      name: 'User 8',
      email: 'user8@gmail.com',
      isCustomer: false,
      isMerchant: false,
    },
    {
      id: 9,
      name: 'User 9',
      email: 'user9@gmail.com',
      isCustomer: false,
      isMerchant: false,
    }
  ],
  userInfo: {
    isLoggedIn: false,
    isSignedUp: false,
    hasSearched: false,
    name: '',
    productTitleSearched: '',
    isAdmin : false,
    claims: '',
    isEmailVerified: false,
    token : "",
    refreshToken : "",
    tokenExpiry : "",
    email : "",
    isCustomer : false,
    isMerchant : false,
    accessToken : null,
    idToken: null,
    uid : null,
  },
  systemInfo: {
    openLoginModal: false,
    openSignupModal: false,
    openCheckoutModal: false,
    openProfileModal: false
  },
  users: null,
  user : null,
  isUserSelected: false,
  selectedUser: null,
  usersAccount: null,
  xsrfToken: null,
  merchantproducts: [],
  selectMerchantProduct: null,
  isProductSelected: false,
  usersAccountDetails: [
      {
          "uid": "NVlOnSK1DGZSDCCpst05HohyoES2",
          "email": "vegancommercetester@gmail.com",
          "phoneNumber": null,
          "emailVerified": true,
          "displayName": null,
          "photoUrl": null,
          "disabled": false,
          "tokensValidAfterTimestamp": 1633502043000,
          "userMetadata": {
              "creationTimestamp": 1633502043045,
              "lastSignInTimestamp": 1634055501703,
              "lastRefreshTimestamp": 1634055501703
          },
          "customClaims": {},
          "passwordHash": "0SR18U_wW45gRgYUF_OvW1MmWsSx8MimHOS2CA1SnRjqH95JSIq6AAVGhB7QSymSA6M1GdXR5wABL5dyQr07tg==",
          "passwordSalt": "iePkDobWHX6n7Q==",
          "providerId": "firebase",
          "providerData": [
              {
                  "uid": "vegancommercetester@gmail.com",
                  "displayName": null,
                  "email": "vegancommercetester@gmail.com",
                  "phoneNumber": null,
                  "photoUrl": null,
                  "providerId": "password"
              }
          ]
      },
      {
          "uid": "bN43MdqsrwbYcwK1NzNgHy1vGnJ3",
          "email": "merchant@test.com",
          "phoneNumber": null,
          "emailVerified": false,
          "displayName": null,
          "photoUrl": null,
          "disabled": false,
          "tokensValidAfterTimestamp": 1633130742000,
          "userMetadata": {
              "creationTimestamp": 1633130742079,
              "lastSignInTimestamp": 1633255345842,
              "lastRefreshTimestamp": 1633255345842
          },
          "customClaims": {
              "claims": [
                  "ROLE_MERCHANT"
              ]
          },
          "passwordHash": "APvNKIlPNsZVdVDe5OsO0fo1f750tSCyhRqnUFpMdZBbCqFXTsEo3Wz0Sa8zJ6m25Aso8wfxVESq1n_btHzuhg==",
          "passwordSalt": "4OSyW0P4U8k1OA==",
          "providerId": "firebase",
          "providerData": [
              {
                  "uid": "merchant@test.com",
                  "displayName": null,
                  "email": "merchant@test.com",
                  "phoneNumber": null,
                  "photoUrl": null,
                  "providerId": "password"
              }
          ]
      },
      {
          "uid": "kIqrdEhcZ4cMl0cPbNjgFdrpMcW2",
          "email": "user@test.com",
          "phoneNumber": null,
          "emailVerified": false,
          "displayName": null,
          "photoUrl": null,
          "disabled": false,
          "tokensValidAfterTimestamp": 1633130726000,
          "userMetadata": {
              "creationTimestamp": 1633130726814,
              "lastSignInTimestamp": 1634078095661,
              "lastRefreshTimestamp": 1634078095661
          },
          "customClaims": {
              "claims": [
                  "ROLE_USER",
                  "ROLE_USER1"
              ]
          },
          "passwordHash": "n959Fjn9qXDNfSfkKscDEmG4oZRyGqiR3n53mWtw9XUeUCz5fs64Hx9HFAoj6CWjFEOh0aQosaOvMAmlDf5wVQ==",
          "passwordSalt": "pQr0dJIiVKfeyA==",
          "providerId": "firebase",
          "providerData": [
              {
                  "uid": "user@test.com",
                  "displayName": null,
                  "email": "user@test.com",
                  "phoneNumber": null,
                  "photoUrl": null,
                  "providerId": "password"
              }
          ]
      },
      {
          "uid": "tCs42hmGcEcrPpAhbpzwyKa6rVn2",
          "email": "admin@test.com",
          "phoneNumber": null,
          "emailVerified": false,
          "displayName": null,
          "photoUrl": null,
          "disabled": false,
          "tokensValidAfterTimestamp": 1633130711000,
          "userMetadata": {
              "creationTimestamp": 1633130711893,
              "lastSignInTimestamp": 1634100923718,
              "lastRefreshTimestamp": 1634100923718
          },
          "customClaims": {
              "claims": [
                  "ROLE_ADMIN"
              ]
          },
          "passwordHash": "kf7mR1OdvCLHKFuHWpXFDUF_g18jvJyDC_xdZBQLBJ8xo8StFLnhSzLAmDmgcDhIypwmb5G3Q-Gwyordkh2noA==",
          "passwordSalt": "gx_3YnFEVSPkCQ==",
          "providerId": "firebase",
          "providerData": [
              {
                  "uid": "admin@test.com",
                  "displayName": null,
                  "email": "admin@test.com",
                  "phoneNumber": null,
                  "photoUrl": null,
                  "providerId": "password"
              }
          ]
      }
  
    ],
})

export const getters = {
  // Product Getters
  productsAdded: state => {
    return state.products.filter(el => {
      return el.isAddedToCart;
    });
  },
  productsAddedToFavourite: state => {
    return state.products.filter(el => {
      return el.isFavourite;
    });
  },
  getProductById: state => id => {
    return state.products.find(product => product.id == id);
  },
  quantity: state => {
    return state.products.quantity;
  },
  //User getter
  isUserLoggedIn: state => {
    return state.userInfo.isLoggedIn;
  },
  isUserSignedUp: state => {
    return state.userInfo.isSignedUp;
  },
  isAdminClaims: state => {
    return state.userInfo.isAdmin;
  },
  getUserName: state => {
    return state.userInfo.name;
  },
  // Modal getter
  isLoginModalOpen: state => {
    return state.systemInfo.openLoginModal;
  },
  isSignupModalOpen: state => {
    return state.systemInfo.openSignupModal;
  },
  isCheckoutModalOpen: state => {
    return state.systemInfo.openCheckoutModal;
  },
  isCustomerProfileModalOpen:  state => {
    return state.systemInfo.openProfileModal;
  },
  // Users 
  getUsers: state => {
    return state.users;
  },
  // authentication
  // add firebase code
  getUser: state => {
    return state.user;
  },
  getUserClaims: state => {
    return state.userInfo.claims;
  },
  getUsersAccount: state => {
    return state.usersAccount;
  },
  getSelectedUser: state =>{
    return state.selectedUser;
  },
/*     getToken: state =>{
    return state.user;
  }, */
  isEmailVerified: state => {
    return state.userInfo.isEmailVerified;
  },
  isCustomerClaims: state => {
    return state.userInfo.isCustomer;
  },
  isMerchantClaims: state => {
    return state.userInfo.isMerchant;
  },
  isUserSelected: state => {
    return state.isUserSelected;
  },
  getUid: state => {
    return state.userInfo.uid;
  },
  getAccessToken : state =>{
    return state.userInfo.accessToken;
  },
  getIdToken : state =>{
    return state.userInfo.idToken;
  },
  getUsersAccountDetails: state =>{
    return state.usersAccountDetails;
  },
  getXsrfToken: state =>{
    return state.xsrfToken;
  },
  getMerchantProducts: state =>{
    return state.merchantproducts;
  },
  getSelectMerchantProduct: state =>{
    return state.selectMerchantProduct;
  },
  isProductSelected: state =>{
    return state.isProductSelected;
  },
}

export const mutations = {
  // Product update
  addToCart: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isAddedToCart = true;
      }
    });
  },
  setAddedBtn: (state, data) => {
    state.products.forEach(el => {
      if (data.id === el.id) {
        el.isAddedBtn = data.status;
      }
    });
  },
  removeFromCart: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isAddedToCart = false;
      }
    });
  },
  removeProductsFromFavourite: state => {
    state.products.filter(el => {
      el.isFavourite = false;
    });
  },
  addToFavourite: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isFavourite = true;
      }
    });
  },
  removeFromFavourite: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isFavourite = false;
      }
    });
  },
  quantity: (state, data) => {
    state.products.forEach(el => {
      if (data.id === el.id) {
        el.quantity = data.quantity;
      }
    });
  },
  // User Update
  isUserLoggedIn: (state, isUserLoggedIn) => {
    state.userInfo.isLoggedIn = isUserLoggedIn;
  },
  isUserSignedUp: (state, isSignedUp) => {
    state.userInfo.isSignedUp = isSignedUp;
  },
  setAdminClaims: (state, isAdmin) => {
    state.userInfo.isAdmin = isAdmin;
  },
  setHasUserSearched: (state, hasSearched) => {
    state.userInfo.hasSearched = hasSearched;
  },
  setUserName: (state, name) => {
    state.userInfo.name = name;
  },
  setProductTitleSearched: (state, titleSearched) => {
    state.userInfo.productTitleSearched = titleSearched;
  },
  // Modal Update
  showLoginModal: (state, show) => {
    state.systemInfo.openLoginModal = show;
  },
  showSignupModal: (state, show) => {
    state.systemInfo.openSignupModal = show;
  },
  showCheckoutModal: (state, show) => {
    state.systemInfo.openCheckoutModal = show;
  },
  showProfileModal: (state, show) => {
    state.systemInfo.openProfileModal = show;
  },
  // users
  setUsers: (state, users) =>{
    state.users = users;
  },
   // add firebase code  
  setUser(state, account) 
  {
    state.user = account;
     // console.log("User State: " + account);
  },
  setUserClaims: (state, claims) => {
    state.userInfo.claims = claims;
  },
  setUsersAccount: (state, accounts) => {
    state.usersAccount = accounts;
  },
  setEmailVerified: (state, isEmailVerified) =>{
    state.userInfo.isEmailVerified = isEmailVerified;
  },
  setCustomerClaims: (state, isCustomer) => {
    state.userInfo.isCustomer = isCustomer;
  },
  setMerchantClaims: (state, isMerchant) => {
    state.userInfo.isMerchant = isMerchant;
  },
  setCustomer: (state, data) => {
    state.allusers.forEach(el => {
      if (data.id === el.id) {
        el.isCustomer = data.status;
      }
    });
  },
  setMerchant: (state, data) => {
    state.allusers.forEach(el => {
      if (data.id === el.id) {
        el.isMerchant = data.status;
      }
    });
  },
  setUserSelected: (state, isSelected) => {
    state.isUserSelected = isSelected;
  },
  setUid: (state, uid) => {
    state.userInfo.uid = uid;
  },
  setAccessToken: (state, accessToken) =>{
    state.userInfo.accessToken = accessToken;
  },
  setSelectedUser: (state, selectedUser) =>{
    state.selectedUser = selectedUser;
  },
  setXsrfToken: (state, token) =>{
    state.xsrfToken = token;
  },
  setMerchantProducts: (state, products) =>{
    state.merchantproducts = products;
  },
  setSelectMerchantProduct: (state, product) =>{
    state.selectMerchantProduct = product;
  },
  setProductSelected: (state, selected) =>{
    state.isProductSelected = selected;
  },
  setIdToken: (state, idToken) =>{
    state.userInfo.idToken = idToken;
  },

}

export const actions = {

  data () {
    return {
      token: null,

    }
  },

  async login({commit}, account){

    let response = null;

    console.log("Start of Firebase SignIn :");
    response = await firebase.auth().signInWithEmailAndPassword(account.email, account.password)
      .then((userCredentials) =>{
        var authUser = userCredentials.user;
        // save Credentials to localStorage
        // localStorage.setItem("credentials", JSON.stringify(authUser.toJSON()));
        // localStorage.setItem("encoded_accessToken", JSON.stringify(authUser.toJSON().stsTokenManager.accessToken));
        //  console.log("credentials :" + JSON.stringify(authUser.toJSON()));
        //  console.log("user :" + JSON.stringify(userCredentials.user));
        //  console.log("Token Manager : " + JSON.stringify(authUser.toJSON().stsTokenManager));
/*         firebase.auth().currentUser.getIdToken().then((idToken) => {
          // Send token to your backend via HTTPS
          // ...
          let token = idToken;
          console.log("ID Token : " + token);
          commit('setIdToken', token);
        }) */
         //console.log("ID Token : " + idToken);
        commit('setUid', JSON.stringify(authUser.toJSON().uid));
        commit('setAccessToken', JSON.stringify(authUser.toJSON().stsTokenManager.accessToken));
        //localStorage.setItem("Id Token", idToken);

        //if (authUser.emailVerified){

          commit('setEmailVerified', authUser.emailVerified);
          localStorage.setItem("emailVerified", true);
          localStorage.setItem("isUserLoggedIn", true);
          let jwtToken = authUser.toJSON().stsTokenManager.accessToken;
          //console.log("JWT Token :" + jwtToken);
          // save User Claims to store for other Vue pages
          let claimsString = JSON.stringify(getUserClaims(jwtToken));
          commit('setUserClaims', claimsString);
          //console.log("Claims : " + JSON.parse(claimsArray), "Length:" + JSON.parse(claimsArray.length));
          let claimsArray = JSON.parse(claimsString);
          claimsArray.forEach(storeUserClaims);

          //response = userCredentials;
          let resp = 'success';
          console.log("End of async Login with Resp" + resp);
          return resp;
          // remove comment when admin user able to set claims
/*         }else{

          localStorage.clear();
          window.alert("Email verification required prior sign in. Refer to your mail box.");
          commit('setEmailVerified', false);

        } */

      }).catch((error)=>{

        localStorage.clear();
        console.log("Auth Error :" + error);
        alert(error.message);
        commit('isUserLoggedIn', false);
        return error;
      })
      return response;

  },

  async register({commit}, account){

    await firebase.auth().createUserWithEmailAndPassword(account.email, account.password)
      .then((userCredentials)=>{
        var authUser = userCredentials.user;
        //console.log("User ID:" + JSON.stringify(authUser.uid));

        let jwtToken = authUser.toJSON().stsTokenManager.accessToken;
        //console.log("JWT token :" + jwtToken);
        let customer = {
          "customerAddress": "",
          "customerBillAddress": "",
          "customerCreditCardNum": "",
          "customerDOB": "",
          "customerEmail": authUser.email,
          "customerFirstName": "",
          "customerId": 0,
          "customerJoinDate": "",
          "customerLastName": "",
          "customerLastUpdate": "",
          "customerLoyaltyPoints": 0,
          "customerMobile": "",
          "customerPostalCode": "",
          "customerUID": authUser.uid
        };

        //console.log("Customer Data :" + JSON.stringify(customer));
        let response = CustomerService.createCustomer(customer, jwtToken);

        //console.log("response from Customer create :" + JSON.stringify(response.data));

        console.log("Signed up successfully!");
        sendVerification();

      }).catch(error=>{
        console.log(error);
      })
      
  },

  async logout(){
    await firebase.auth().signOut();
    localStorage.clear();

    location.href = "/";
    setTimeout(() =>
    {
      this.isError = false;
    }, 3000);

  },

  async getAllUsers() {

    localStorage.getItem('user-token', token);
    console.log("User Token: " +JSON.stringify(token));

    try{

      axios.setToken(token);
      const fetchData = await axios.get('/api1/v1' + '/customer');
      this.$store.commit('setUsers', fetchData.data);

      return fetchData;

    } catch (err){

      setRequestError (err.message);
 
    }
  },
}

/* 
export const actions = {
  async nuxtServerInit({ commit }, { Request }) {
    // const res = await this.$axios.get("/api/current_user")
    // commit("SET_USER", res.data)
    if (process.server && process.static) return;
    if (!Request.headers.cookie) return;

    const parsed = cookieParser.parsed(Request.headers.cookie);
    const accessTokenCookie = parsed.access_token;

    if (!accessTokenCookie) return;

    const decoded = JWTDecode(accessTokenCookie);

    if (decoded){
      commit("SET_USER", {
        uid: decoded.user_id,
        email: decoded.email
      });
    }

  },

  async logout({ commit }) {
    const { data } = await this.$axios.get("/api/logout")
    if (data.ok) commit("SET_USER", null)
  },

  async handleToken({ commit }, token) {
    const res = await this.$axios.post("/api/stripe", token)
    commit("SET_USER", res.data)
  }
}  */
